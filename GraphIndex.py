from SpatialIndex import SpatialIndex
from Data import Data
from Edge import Edge
import numpy as np


def rectangular_to_polar(x1, y1, x2, y2):  # 直角坐标转极坐标
    dx = x2 - x1
    dy = y2 - y1
    d = np.sqrt(np.square(dx) + np.square(dy))
    theta = np.arctan2(dy, dx)
    return theta, d


@DeprecationWarning
def polar_to_rectangular(r, theta):  # 极坐标转直角坐标
    theta = theta * (np.pi / 180)
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    return round(x, 2), round(y, 2)


def get_anchor_vertex():
    return 0


class GraphIndex:
    diagonal = 1080
    granularity = 2
    pairs = {}

    def __init__(self, d):
        self.diagonal = d

    def discretization(self, theta, d):

        d /= self.diagonal
        return np.around([theta, d], decimals=self.granularity)

    def find_index(self, s, e, theta, d) -> int:
        index = 0
        spatial_list: list = self.pairs.get((s, e))
        # if spatial_list is None:
        while index < len(spatial_list):
            i = spatial_list[index]
            if (i.theta, i.d) == (theta, d):
                break
            index += 1
        if index == len(spatial_list):
            index = -1
        return index

    def update_index(self, fid, data):
        anchor: Data = data[get_anchor_vertex()]
        del data[get_anchor_vertex()]
        for datum in data:
            theta, d = rectangular_to_polar(anchor.x, anchor.y, datum.x, datum.y)
            theta, d = self.discretization(theta, d)

            spatial_list: list = self.pairs.get((anchor.label, datum.label))
            index = 0
            # if (anchor.label, datum.label) not in self.pairs:
            if spatial_list is None:
                spatial_list = [SpatialIndex(theta, d)]
                self.pairs[(anchor.label, datum.label)] = spatial_list
            else:
                index = self.find_index(anchor.label, datum.label, theta, d)
                if -1 == index:
                    spatial_list.append(SpatialIndex(theta, d))
            spatial_list[index].add_vertex_frame(anchor.oid, datum.oid, fid)

    def query_frames(self, sl, el, theta, d) -> set:
        result = set()
        for i in self.pairs[(sl, el)]:
            i: SpatialIndex
            if (theta, d) == (i.theta, i.d):
                result = result.union(i.get_frames())
                break
        return result

    def edge_match(self, edge: Edge, fid):
        result = set()
        sl = edge.s.label
        el = edge.e.label
        theta = edge.radian
        d = edge.d
        for i in self.pairs[(sl, el)]:
            i: SpatialIndex
            if (theta, d) == (i.theta, i.d):
                result = i.frame_filter(fid)
                break
        return result
