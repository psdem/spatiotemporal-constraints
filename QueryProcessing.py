from GraphIndex import GraphIndex
from Graph import Graph
# from Vertex import Vertex
from Edge import Edge


def window_generation(query_graph_sequence: list, gi: GraphIndex, size: int) -> set:
    result = set()
    for graph in query_graph_sequence:
        graph: Graph
        for vertex in graph.get_vertices():
            for edge in vertex.connectedTo.values():
                # s: Vertex = graph.get_vertex(edge.s)
                # e = graph.get_vertex(edge.e)
                # result = result.union(gi.query_frames(s.label, e.label, edge.radian, edge.d))
                result = result.union(gi.query_frames(edge.s.label, edge.e.label, edge.radian, edge.d))
    tmp = result.copy()
    for frame in tmp:
        for j in range(size):
            result.add(frame - j)
    return result


def spatial_match(query_graph: Graph, fid: int, gi: GraphIndex):
    qe = []
    for vertex in query_graph.get_vertices():
        for edge in vertex.connectedTo.values():
            qe.append(edge)
    res = {}
    idx = 0
    while idx < len(qe):
        e: Edge = qe[idx]
        sfe = gi.edge_match(e, fid)
        for s in sfe:
            s: Edge
            vs = s.s
            ve = s.e
            i = res.get(vs)
            if i is None:
                i = [{vs}]
                res[vs] = i
            for j in range(idx + 1):
                i.append(set())
            i[idx + 1] = i[idx + 1].union({ve})
        idx += 1
    r = res.values()
    index = 0
    for i in r:
        for element in i:
            if not element:
                key = list(res.keys())[index]
                del res[key]
        index += 1
    return r  # return res
