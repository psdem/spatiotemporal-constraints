# This is a sample Python script.
from Graph import Graph
from Data import Data
from GraphIndex import GraphIndex

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def build_graph(word_file):
    d = {}
    graph = Graph()
    file = open(word_file, 'r')
    for line in file:
        word = line[:-1]
        for j in range(len(word)):
            bucket = word[:j] + '_' + word[j + 1:]
            if bucket in d:
                d[bucket].append(word)
            else:
                d[bucket] = [word]
        # 同一个桶的单词建立边
        for bucket in d.keys():
            for word1 in d[bucket]:
                for word2 in d[bucket]:
                    if word1 != word2:
                        graph.add_edge(word1, word2)
        return graph


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    data1 = Data(10145, "car", 555, 666)
    data2 = Data(54321, "people", 599, 611)
    data3 = Data(50102, "bus", 123, 456)
    data = [data1, data2, data3]
    fid = 3
    g = GraphIndex(1080)
    g.update_index(fid, data)
