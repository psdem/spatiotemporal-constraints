from Vertex import Vertex


class Graph:
    def __init__(self):
        self.vertList = {}
        self.numVertices = 0

    # 新加顶点
    def add_vertex(self, key, label):
        self.numVertices = self.numVertices + 1
        new_vertex = Vertex(key, label)
        self.vertList[key] = new_vertex
        return new_vertex

    # 通过key查找顶点
    def get_vertex(self, key):
        if key in self.vertList:
            return self.vertList[key]
        else:
            return None

    def __contains__(self, key):
        return key in self.vertList

    # def add_edge(self, f, t, cost=0):
    #     if f not in self.vertList:  # 不存在的顶点先添加
    #         nv = self.add_vertex(f)
    #     if t not in self.vertList:
    #         nv = self.add_vertex(t)
    #     self.vertList[f].add_neighbor(self.vertList[t], cost)
    #
    def get_vertices(self):
        return self.vertList

    def __iter__(self):
        return iter(self.vertList.values())
