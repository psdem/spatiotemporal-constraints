class Edge:
    def __init__(self, u, v):
        self.s = v
        self.e = u
        self.radian = 0
        self.d = 0

    def __eq__(self, other):
        return self.s, self.e == other.s, other.e
