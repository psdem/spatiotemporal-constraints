from FrameOfVertex import FrameOfVertex
from Edge import Edge


class SpatialIndex:
    def __init__(self, radian, distance):
        self.theta = radian
        self.d = distance
        self.vertex_pairs = []

    def find_index(self, s, e) -> int:
        index = 0
        while index < len(self.vertex_pairs):
            i: FrameOfVertex = self.vertex_pairs[index]
            p = i.vertexPairs
            if (p.s, p.e) == (s, e):
                break
            index += 1
        if index == len(self.vertex_pairs):
            index = -1
        return index

    def add_vertex_frame(self, s, e, fid):
        index = self.find_index(s, e)
        if -1 == index:
            tmp = FrameOfVertex(Edge(s, e), fid)
            self.vertex_pairs.append(tmp)
        else:
            i: FrameOfVertex = self.vertex_pairs[index]
            i.frameIds.append(fid)

    def get_frames(self) -> set:
        result = set()
        for j in self.vertex_pairs:
            result.union(set(j.frameIds))
        return result

    def frame_filter(self, fid) -> set:
        result = set()
        for i in self.vertex_pairs:
            if fid in i.frameIds:
                result.add(i.vertexPairs)
        return result
