from Edge import Edge


class Vertex:
    def __init__(self, key, label):
        self.id = key
        self.label = label
        self.connectedTo = {}

    # 从这个顶点添加一个连接到另一个
    def add_neighbor(self, nbr, edge: Edge):  # nbr是顶点对象的key
        self.connectedTo[nbr] = edge

    # 顶点数据字符串化，方便打印
    def __str__(self):
        return str(self.id) + ' connectedTo: ' + str([x.id for x in self.connectedTo.keys()])

    # 返回邻接表中的所有顶点
    def get_connections(self):
        return self.connectedTo.keys()

    # 返回key
    def get_id(self) -> int:
        return self.id

    # 返回顶点边的权重。
    def get_weight(self, nbr: int):
        return self.connectedTo[nbr]
